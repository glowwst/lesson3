/*
Создайте класс Car у которого будут следующие поля: модель, скорость, вес, цена, расход по километражу. Все поля кроме модели задать рандомно, можно использовать класс Int.random.  Создать 4 машины и найти какая среди машин будет самая быстрая, самая дешёвая. Дать возможность юзеру назначить дистанцию(совершить гонку) и получить среди всех машин ту которая проедет бысстрее всех(учитывая все их характеристики), ту которая проедет медленнее всех, ту у которой первой закончится бензин. Показать список после гонки в формате 1) место - и тд. Результат продемонстрировать в консоли.
*/

class Car {  
    let model: String
    let speedKmH: Double
    var weightKg: Int
    var priceUSD: Double
    var fuelConsumption: Double
    var tank: Int
    
    init(model: String, speedKmH: Double, weightKg: Int, priceUSD: Double , fuelConsumption:Double, tank: Int) {
      self.model = model
      self.speedKmH = speedKmH
      self.weightKg = weightKg
      self.priceUSD = priceUSD
      self.fuelConsumption = fuelConsumption
      self.tank = tank
    }
  
    func aboutCarInfo() {
        print("Модель машины - \(model), скорость - \(speedKmH) Км/ч, вес - \(weightKg) кг, price - \(priceUSD) $, расход топлива на 100 км - \(fuelConsumption), объём бака - \(tank) л.")
    }
}

var carArray: [Car] = []
let car1 = Car(model: "Tesla", speedKmH: Double.random(in: 150.0 ..< 320.0), weightKg: Int.random(in: 1200 ..< 2000), priceUSD:  Double.random(in: 50000.0 ..< 320000.0), fuelConsumption: Double.random(in: 6.0 ..< 40.0), tank: Int.random(in: 40 ..< 80))
let car2 = Car(model: "Volvo", speedKmH: Double.random(in: 150.0 ..< 320.0), weightKg: Int.random(in: 1200 ..< 2000), priceUSD:  Double.random(in: 50000.0 ..< 320000.0), fuelConsumption: Double.random(in: 6.0 ..< 40.0), tank: Int.random(in: 40 ..< 80))
let car3 = Car(model: "BWM", speedKmH: Double.random(in: 150.0 ..< 320.0), weightKg: Int.random(in: 1200 ..< 2000), priceUSD:  Double.random(in: 50000.0 ..< 320000.0), fuelConsumption: Double.random(in: 6.0 ..< 40.0), tank: Int.random(in: 40 ..< 80))
let car4 = Car(model: "Mercedes", speedKmH: Double.random(in: 150.0 ..< 320.0), weightKg: Int.random(in: 1200 ..< 2000), priceUSD:  Double.random(in: 50000.0 ..< 320000.0), fuelConsumption: Double.random(in: 6.0 ..< 40.0), tank: Int.random(in: 40 ..< 80))

carArray = [car1, car2, car3, car4]

print ("Первочальный массив объектов:")

carArray.forEach { car in 
  car.aboutCarInfo()
}

print ("\n")
print ("Самая быстрая машина:")
let fastestCar = carArray.sorted(by: {$0.speedKmH > $1.speedKmH })
print(fastestCar.first?.aboutCarInfo())

print ("\n")
print ("Самая дешевая машина:")
let cheapestCar = carArray.sorted(by: {$1.priceUSD > $0.priceUSD })
print(cheapestCar.first?.aboutCarInfo())
print ("\n")

// Гонка

func race (cars: [Car], distance: Double) {
var turpleCars: [(Car, Double)] = []
for car in cars  {
  let raceTime: Double =  distance / ( Double(car.speedKmH) / 60 )
  let fuelRequired = distance * (car.fuelConsumption / 100)
  if fuelRequired < Double(car.tank) {
      turpleCars.append((car, raceTime))
  } else {
    print("\(car.model) - не участвует в гонке")
  }
}
turpleCars.sort(by: {$0.1 < $1.1})
turpleCars.forEach { car, raceTime in 
  print(car.model," проехала", raceTime, " минут")
}
}

// Запуск гонки

race(cars: carArray, distance: 200)






