import Foundation

struct Fighter {

    let name: String

    // Добавил ХП, пусть пиздятся до смерти))

		var hp: Double

    // Урон наносимый частью тела
		
		let damageLeftHand: Double
		let damageRightHand: Double
		let damageLeftLeg: Double
		let damageRightLeg: Double
		let damageHead: Double

    // Шанс от 0 до 100% получить урон по этой части тела

		let СhanceToDoDamageLeftHand: Double
		let СhanceToDoDamageRightHand: Double
		let СhanceToDoDamageLeftLeg: Double
		let СhanceToDoDamageRightLeg: Double
		let СhanceToDoDamageTorso: Double
		let СhanceToDoDamageHead: Double

    // модификатор урона почасти тела (насколько удар по этой части увеличивает урон)

		let damageModifierLeftHand: Double
		let damageModifierRightHand: Double
		let damageModifierLeftLeg: Double
		let damageModifierRightLeg: Double
		let damageModifierTorso: Double
		let damageModifierHead: Double

    init(name: String, hp: Double, damageLeftHand: Double, damageRightHand: Double, damageLeftLeg: Double, damageRightLeg: Double, damageHead: Double, СhanceToDoDamageLeftHand: Double, СhanceToDoDamageRightHand: Double, СhanceToDoDamageLeftLeg: Double, СhanceToDoDamageRightLeg: Double, СhanceToDoDamageTorso: Double, СhanceToDoDamageHead: Double, damageModifierLeftHand: Double, damageModifierRightHand: Double, damageModifierLeftLeg: Double, damageModifierRightLeg: Double, damageModifierTorso: Double, damageModifierHead: Double) {

				self.name = name
				
				self.hp = hp
				
				self.damageLeftHand = damageLeftHand
				self.damageRightHand = damageRightHand
				self.damageLeftLeg = damageLeftLeg
				self.damageRightLeg = damageRightLeg
				self.damageHead = damageHead
				
				self.СhanceToDoDamageLeftHand = СhanceToDoDamageLeftHand
				self.СhanceToDoDamageRightHand = СhanceToDoDamageRightHand
				self.СhanceToDoDamageLeftLeg = СhanceToDoDamageLeftLeg
				self.СhanceToDoDamageRightLeg = СhanceToDoDamageRightLeg
				self.СhanceToDoDamageTorso = СhanceToDoDamageTorso
				self.СhanceToDoDamageHead = СhanceToDoDamageHead
				
				self.damageModifierLeftHand = damageModifierLeftHand
				self.damageModifierRightHand = damageModifierRightHand
				self.damageModifierLeftLeg = damageModifierLeftLeg
				self.damageModifierRightLeg = damageModifierRightLeg
				self.damageModifierTorso = damageModifierTorso
				self.damageModifierHead = damageModifierHead
    }

    // Я котик и у меня лапки, поэтому я не смог написать все поля
    func aboutFighter () {
      print("Боец  - \(name) ХП - \(hp)")
    }

    func makePunch () -> Double {
      let bodyPartForHit: Int = Int.random(in: 1 ..< 5) 
      switch bodyPartForHit {
        case 1:
          return damageLeftHand * 3
        case 2:
          return damageRightHand * 3
        case 3:
          return damageLeftLeg * 3
        case 4:
          return damageRightLeg * 3
        case 5:
          return damageHead * 3
        default:
          return damageLeftHand * 3
      }
    }

      func takeDamage () -> Double {
      var damage: Double = 0
      for _ in 1...3 { switch Int.random(in: 1 ..< 6) {
        case 1:
          damage += СhanceToDoDamageLeftHand * damageModifierLeftHand
        case 2:
          damage += СhanceToDoDamageRightHand * damageModifierRightHand
        case 3:
          damage += СhanceToDoDamageLeftLeg * damageModifierLeftLeg
        case 4:
          damage += СhanceToDoDamageRightLeg * damageModifierRightLeg
        case 5:
          damage += СhanceToDoDamageTorso * damageModifierTorso
        case 6:
          damage += СhanceToDoDamageHead * damageModifierHead
        default:
          damage += СhanceToDoDamageLeftHand * damageModifierLeftHand   
      }
      }
      return damage
    }

}

    // Генерация имени
func randomString(length: Int) -> String {
    let letters = "абвгдеёжз​ийклмнопрстуфхцчшщъыьэюя0123456789"
    var s = ""
    for _ in 0 ..< length {
        s.append(letters.randomElement()!)
    }
    return s
}

var fighterArray: [Fighter] = []

for _ in  0...20 {
  fighterArray.append(Fighter(name: randomString(length: 10), hp: Double(round(10*Double.random(in: 150.0 ..< 320.0))/10), damageLeftHand: Double.random(in: 5.0 ..< 20.0), damageRightHand: Double.random(in: 5.0 ..< 20.0), damageLeftLeg: Double.random(in: 5.0 ..< 20.0), damageRightLeg: Double.random(in: 5.0 ..< 20.0), damageHead: Double.random(in: 5.0 ..< 20.0), СhanceToDoDamageLeftHand: Double.random(in: 0 ..< 1), СhanceToDoDamageRightHand: Double.random(in: 0 ..< 1), СhanceToDoDamageLeftLeg: Double.random(in: 0 ..< 1), СhanceToDoDamageRightLeg: Double.random(in: 0 ..< 1), СhanceToDoDamageTorso: Double.random(in: 0 ..< 1), СhanceToDoDamageHead: Double.random(in: 0 ..< 1), damageModifierLeftHand: Double.random(in: 0.5 ..< 5), damageModifierRightHand: Double.random(in: 0.5 ..< 5), damageModifierLeftLeg: Double.random(in: 0.5 ..< 5), damageModifierRightLeg: Double.random(in: 0.5 ..< 5), damageModifierTorso: Double.random(in: 0.5 ..< 5), damageModifierHead: Double.random(in: 0.5 ..< 5)))
}

fighterArray.forEach { fighter in 
  fighter.aboutFighter()
}

print("\n")
print("Пиздилка!!!:")

// Драка
func roundOfFight(fighters: [Fighter]) {
  var round: Int = 1
  var counter: Int = 0
  var winner: Int = 1;
  var j: Int = 0
  for i in fighters {
    if  counter != fighters.count {
        j = 0
        for j in counter...fighters.count {
        print("Боец - \(round)")
        print("\(i.name) против \(fighters[j+1].name)")
        winner = oneToOne(fighterLeftF: i, fighterRightF: fighters[j+1])
        if winner == 2 { break}
        }
    }
    counter += 1
    round += 1
  }
}

func oneToOne (fighterLeftF: Fighter,fighterRightF: Fighter ) -> Int {
var fighterLeft = fighterLeftF
var fighterRight = fighterRightF
var positionLeft: Bool = true
while (fighterLeft.hp < 0 || fighterRight.hp < 0 ) {
    if positionLeft == true {
      fighterRight.hp -=  fighterLeft.makePunch() * fighterRight.takeDamage()
      positionLeft = false
    } else {
      fighterLeft.hp -=  fighterRight.makePunch() * fighterRight.takeDamage()
      positionLeft = true
    }
}
if fighterLeft.hp > fighterRight.hp {
  print("Победил - \(fighterLeft.name)")
  return 1
} else {
  print("Победил - \(fighterRight.name)")
  print("")
  return 2
}
}

roundOfFight(fighters: fighterArray)